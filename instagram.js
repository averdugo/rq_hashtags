const axios = require('axios').default;
var xml = require('xml');
const fs = require("fs")


let time = process.env.npm_config_time
let tag = process.env.npm_config_tag

var getUserId = function(id) {
    var url= `https://www.instagram.com/graphql/query/?query_hash=c9100bf9110dd6361671f113dd02e7d6&variables={"user_id":"${id}","include_chaining":false,"include_reel":true,"include_suggested_users":false,"include_logged_out_extras":false,"include_highlight_reels":false,"include_related_profiles":false}`;
    console.log(url);
    axios.get(url).then(function (response) {
        console.log(response.data);
        /*var userInfo = response.data
        return userInfo.data.user.reel.user.username*/
    })
    .catch(function (error) {
        console.log('axios error on get userid');
    })

}

var createXml = function(data) {
    var today = new Date().toString();
    var xmlData = [ 
        { 
            rss: [ 
                { 
                    _attr: { 'xmlns:dc':"http://purl.org/dc/elements/1.1/", 'xmlns:content':"http://purl.org/rss/1.0/modules/content/", 'xmlns:atom':"http://www.w3.org/2005/Atom", 'xmlns:media':"http://search.yahoo.com/mrss/"} 
                }, 
                { 
                    channel: [ 
                        { 
                            title: {
                                _cdata: `#${tag} hashtag on Instagram`
                            }
                        }, 
                        { 
                            description: { 
                                _cdata: `6 Posts - See Instagram photos and videos from ‘${tag}’ hashtag`
                            } 
                        },
                        { 
                            link: `https://www.instagram.com/explore/tags/${tag}/`
                        },
                        { 
                            image: [
                                {
                                    url: 'https://www.instagram.com/static/images/ico/favicon-192.png/68d99ba29cc8.png'
                                },
                                {
                                    title: `#${tag} hashtag on Instagram`
                                },
                                {
                                    link: `https://www.instagram.com/explore/tags/${tag}/`
                                },
                            ]
                        },
                        {
                            generator: 'https://ramiroquartz.com/'
                        },
                        {
                            lastBuildDate : today
                        },
                        {
                            language: {
                                _cdata: 'en'
                            }
                        },
                        data[0],
                        data[1],
                        data[2],
                        data[3],
                        data[4],
                        data[5],
                        data[6],
                        
                        
                    ] 
                   
                }
            ] 
        } 
    ];
    
    fs.writeFile("instagram/instagram.xml", xml(xmlData, true), function(err, result) {
        if (err) {
            console.log("Xml save error", err)
        } else {
            console.log("Xml file successfully updated.")
        }
    })
}

var jsonFormat = function(data) {

    var nodes = data.graphql.hashtag.edge_hashtag_to_media.edges;
    if (nodes.length === 0) {
        return false;
    }
    var items = [];
    nodes.forEach(n => {
        var comment = n.node.edge_media_to_caption.edges[0].node.text;
        var img =  n.node.display_url;
        var shortcode = n.node.shortcode;
        var publishDate = new Date(n.node.taken_at_timestamp * 1000).toString();
        console.log(n.node.accessibility_caption);
        var username= "Instagram";
        if(n.node.accessibility_caption) {
            var tString = n.node.accessibility_caption.replace('Photo by ', '');
            var username = tString.split(' ');
        }
        var temp = {
            item: [
                {title: {_cdata: comment}}, 
                {description: { _cdata: `<div><img src="${img}" style="width: 100%;"><div>${comment}</div></div>`}},
                {link: `https://www.instagram.com/p/${shortcode}`},
                {guid:[ { _attr: { isPermaLink: 'false' } }, 'a8a28be6ca9be278afb3c5957f288d5f'] },
                {'dc:creator' : {_cdata: username[0]}}, 
                {pubDate: publishDate},
                {'media:content' : { _attr: { medium: 'image',url: img,width: '1080',heigth: '809'}}}
            ]
        }
        items.push(temp)
    });

    createXml(items, items.length);
}

setInterval(function(){
    axios.get(`https://www.instagram.com/explore/tags/${tag}/?__a=1`).then(function (response) {
        jsonFormat(response.data)
    })
    .catch(function (error) {
        console.log('axios error', error);
    })
},time);







