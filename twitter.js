const axios = require('axios').default;
var xml = require('xml');
const fs = require("fs")


let time = process.env.npm_config_time
let tag = process.env.npm_config_tag
let url = `https://api.twitter.com/2/tweets/search/recent?query=%23${tag}&tweet.fields=author_id,context_annotations,created_at,entities,id,lang,text&expansions=author_id,attachments.media_keys&user.fields=username,profile_image_url,name&media.fields=duration_ms,url`;

var createXml = function(data) {
    var today = new Date().toString();
    var xmlData = [ 
        { 
            rss: [ 
                { 
                    _attr: { 'xmlns:dc':"http://purl.org/dc/elements/1.1/", 'xmlns:content':"http://purl.org/rss/1.0/modules/content/", 'xmlns:atom':"http://www.w3.org/2005/Atom", 'xmlns:media':"http://search.yahoo.com/mrss/"} 
                }, 
                { 
                    channel: [ 
                        { 
                            title: {
                                _cdata: `#${tag} - Twitter Search / Twitter`
                            }
                        }, 
                        { 
                            description: { 
                                _cdata:  `#${tag} - Twitter Search / Twitter`
                            } 
                        },
                        { 
                            link: `https://mobile.twitter.com/search?q=%23${tag}`
                        },
                        { 
                            image: [
                                {
                                    url: 'https://abs.twimg.com/favicons/favicon.ico'
                                },
                                {
                                    title: `#${tag} - Twitter Search / Twitter`
                                },
                                {
                                    link: `https://mobile.twitter.com/search?q=%23${tag}`
                                },
                            ]
                        },
                        {
                            generator: 'https://ramiroquartz.com/'
                        },
                        {
                            lastBuildDate : today
                        },
                        {
                            language: {
                                _cdata: 'en'
                            }
                        },
                        data[0],
                        data[1],
                        data[2],
                        data[3],
                        data[4],
                        data[5],
                        data[6],
                        
                        
                    ] 
                   
                }
            ] 
        } 
    ];
    
    fs.writeFile("twitter/twitter.xml", xml(xmlData, true), function(err, result) {
        if (err) {
            console.log("Xml save error", err)
        } else {
            console.log("Xml file successfully updated.")
        }
    })
}

var jsonFormat = function(data) {

    var nodes = data.data;
    var metadata = data.includes;
    if (nodes.length === 0) {
        return false;
    }
    var items = [];
    nodes.forEach(n => {
        var user = metadata.users.find(u => u.id === n.author_id);
        var comment = n.text;
        var tweetUrl = n.entities.urls ? n.entities.urls[0].url : `https://twitter.com/${user.username}/status/${n.id}`
        var publishDate = n.created_at;
        
        var description = `<blockquote class="twitter-tweet" data-width="550"><p lang="es" dir="ltr">${comment}<a href="https://mobile.twitter.com/search?q=%${tag}">#${tag}</a> <a href="${tweetUrl}">${tweetUrl}</a></p>${user.username}<a href="${tweetUrl}">${publishDate}</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>`;


        var img =  n.entities.urls ? n.entities.urls[0].display_url : user.profile_image_url;
        var temp = {
            item: [
                {title: {_cdata: comment}}, 
                {description: { _cdata: description}},
                {link: tweetUrl},
                {guid:[ { _attr: { isPermaLink: 'false' } }, n.id] },
                {'dc:creator' : {_cdata: user.username}}, 
                {pubDate: publishDate},
                {enclosure: { _attr: { url: img, length: '0',type: 'image/jpeg'}}},
                {'media:content' : { _attr: { medium: 'image',url: img,width: '1080',heigth: '809'}}}
            ]
        }
        items.push(temp)
    });

    createXml(items, items.length);
}

setInterval(function(){
    var headers = {
        headers: {
          Authorization: 'Bearer AAAAAAAAAAAAAAAAAAAAALSiIQEAAAAAc1Ko1Q5h%2Bn4oTwPIPP44r%2BZnIt0%3DkfJ2LUuY2t8Hc6XLUsxMgBmAvOqp4FIMRyBHcNNCLSXKrku4J9'
        }
    }
    axios.get(url, headers ).then(function (response) {
        if(!response.data.data) {
            console.log('No hay twitts');
        }else {
            jsonFormat(response.data)
        }
    })
    .catch(function (error) {
        console.log('axios error', error);
    })
},time);







